import inspect
import os
from pathlib import Path
import unittest
from unittest import mock
from hoppr.configs.credentials import Credentials
from hoppr.exceptions import HopprCredentialsError
from hoppr.hoppr_types.credentials_file_content import CredentialRequiredService

import pytest

class TestCredentials(unittest.TestCase):

    def test_credentials_load(self):
        Credentials.load_file(Path("test","resources","credential","cred-test.yml").resolve())

        assert Credentials.get_content().kind == "Credentials"
        assert Credentials.get_content().metadata.name == "Registry Credentials"
        assert Credentials.get_content().metadata.version == "v1"
        assert Credentials.get_content().metadata.description == "Sample credentials file"

        svc1 = CredentialRequiredService(url='gitlab.com', user='infra-pipeline-auto', user_env=None, pass_env='GITLAB_PW')
        svc2 = CredentialRequiredService(url='some-site.com', user='pipeline-bot', user_env=None, pass_env='SOME_TOKEN')
        svc3 = CredentialRequiredService(url='registry-test.com', user='cis-gitlab', user_env=None, pass_env='DOCKER_PW')

        assert svc1 in Credentials.get_content().credential_required_services
        assert svc2 in Credentials.get_content().credential_required_services
        assert svc3 in Credentials.get_content().credential_required_services

    @mock.patch.dict(os.environ, {"GITLAB_PW": "GITLAB_ENV_TEST_PASSWORD"}, clear=True)
    def test_credentials_find_success(self):
        Credentials.load_file(Path("test","resources","credential","cred-test.yml").resolve())
        personal_cred = Credentials.find_credentials("gitlab.com")
        assert personal_cred.username == 'infra-pipeline-auto'
        assert personal_cred.password == "GITLAB_ENV_TEST_PASSWORD"

    def test_credentials_find_failed(self):
        with pytest.raises(HopprCredentialsError):
            Credentials.load_file(Path("test","resources","credential","cred-test.yml").resolve())
            personal_cred = Credentials.find_credentials("gitlab.com")
            assert personal_cred is None
