import json
import os
from pathlib import Path
import unittest
from unittest import mock
from hoppr.configs.manifest import Manifest
from hoppr.flatten_sboms import flatten_sboms
from hoppr.hoppr_types import manifest_file_content

import pytest

class TestFlattenSboms(unittest.TestCase):
    def test_flatten_sbom_success(self):
        manifest1 = Manifest.load_file(Path("test","resources","manifest","unit","manifest.yml").resolve())

        flattened_sbom = flatten_sboms(manifest1)

        assert len(flattened_sbom.components) == 5

        assert flattened_sbom.components[0].purl == "pkg:apt/@angular-devkit/architect@0.1303.1"
        assert flattened_sbom.components[1].purl == "pkg:apt/@create-react-app/web-app@0.7624.1"
        assert flattened_sbom.components[2].purl == "pkg:pypi/wsgiref@0.1.2"
        assert flattened_sbom.components[3].purl == "pkg:pypi/argparse@1.2.1"
        assert flattened_sbom.components[4].purl == "pkg:apt/@webpack/block@0.9853.2"


        assert len(flattened_sbom.components[0].externalReferences) == 2
        assert len(flattened_sbom.components[1].externalReferences) == 1
        assert len(flattened_sbom.components[2].externalReferences) == 2
        assert len(flattened_sbom.components[3].externalReferences) == 1
        assert len(flattened_sbom.components[4].externalReferences) == 1

        assert flattened_sbom.components[0].externalReferences[0].url == 'test/resources/bom/unit_bom1_mini.json'
        assert flattened_sbom.components[0].externalReferences[1].url == 'test/resources/bom/unit_bom3_mini.json'
        assert flattened_sbom.components[1].externalReferences[0].url == 'test/resources/bom/unit_bom1_mini.json'
        assert flattened_sbom.components[2].externalReferences[0].url == 'test/resources/bom/unit_bom2_mini.json'
        assert flattened_sbom.components[2].externalReferences[1].url == 'test/resources/bom/unit_bom3_mini.json'
        assert flattened_sbom.components[3].externalReferences[0].url == 'test/resources/bom/unit_bom2_mini.json'
        assert flattened_sbom.components[4].externalReferences[0].url == 'test/resources/bom/unit_bom3_mini.json'

        assert len(flattened_sbom.components[0].properties) == 1
        assert len(flattened_sbom.components[1].properties) == 1
        assert len(flattened_sbom.components[2].properties) == 3
        assert len(flattened_sbom.components[3].properties) == 3
        assert len(flattened_sbom.components[4].properties) == 1

    sbom_content = json.loads("""
{
    "bomFormat": "CycloneDX",
    "specVersion": "1.4",
    "serialNumber": "urn:uuid:79190df2-cebf-46d1-b651-681b8b7784e3",
    "version": 1,
    "components": [
        {
            "type": "library",
            "author": "Angular Authors",
            "name": "@angular-devkit/architect",
            "version": "0.1303.1",
            "purl": "pkg:apt/@angular-devkit/architect@0.1303.1"
        }
    ]
}
""")

    @mock.patch("hoppr.net.load_url", return_value=sbom_content)
    def test_flatten_sbom_by_url_success(self, mock_sbom):
        manifest = Manifest.load_file(Path("test","resources","manifest","unit","manifest-url-sbom.yml").resolve())
        sbom = flatten_sboms(manifest)
        assert sbom.components[0].purl == "pkg:apt/@angular-devkit/architect@0.1303.1"
        assert len(sbom.components) == 1
