import inspect
from pathlib import Path
import unittest
from unittest import mock

from hoppr.core_plugins.collect_raw_plugin import CollectRawPlugin
from hoppr.context import Context
from hoppr.configs.credentials import Credentials
from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from test.mock_objects import MockHttpResponse
from hoppr.hoppr_types.cred_object import CredObject


class TestCollectorRaw(unittest.TestCase):

    def _create_test_plugin(self):
        context = Context(manifest="MANIFEST", collect_root_dir="COLLECTION_DIR", consolidated_sbom="BOM", retry_wait_seconds=1, max_processes=3)
        my_plugin = CollectRawPlugin(context=context, config="CONFIG")
        return my_plugin

    @mock.patch.object(Credentials, "find_credentials", return_value= CredObject("fake_user", "fake_pw"))
    @mock.patch.object(CollectRawPlugin, "_get_repos", return_value=["https://somewhere.com"])
    @mock.patch("hoppr.core_plugins.collect_raw_plugin.download_file", return_value=MockHttpResponse(200, content="mocked content"))
    def test_collector_raw_url(self, mock_download, mock_get_repos, mock_find_creds):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    @mock.patch.object(CollectRawPlugin, "_get_repos", return_value=["file://somewhere.com"])
    @mock.patch("shutil.copy")
    @mock.patch.object(Path, "is_file", return_value=True)
    def test_collector_raw_file(self, mock_path_isfile, mock_sh_copy, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    @mock.patch.object(CollectRawPlugin, "_get_repos", return_value=["file://somewhere.com"])
    @mock.patch("shutil.copy")
    @mock.patch.object(Path, "is_file", return_value=False)
    def test_collector_raw_file_not_found(self, mock_path_isfile, mock_sh_copy, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3", type="file")
        collect_result = my_plugin.process_component(comp)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"

    @mock.patch.object(CollectRawPlugin, "_get_repos", return_value=["https://somewhere.com"])
    def test_collect_raw_url_mismatch(self, mock_get_repos):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        my_plugin = self._create_test_plugin()

        comp = Component(name="TestComponent", purl="pkg:generic/something/else@1.2.3?repository_url=my.repo", type="file")
        collect_result = my_plugin.process_component(comp)

        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"

        assert collect_result.message == f"Purl-specified repository url (my.repo) does not match current repo (https://somewhere.com)."

    def test_get_version(self):
        my_plugin = self._create_test_plugin()
        assert len(my_plugin.get_version()) > 0
